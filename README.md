# Canal March App TV

## Requirements

- rnv (`$ npm install rnv -g`)
- node (>= 10.13.0)
- yarn (>= 1.10.0)
- Tizen Studio for tizen (samsung)

## Steps:

- Install dependencies: `$ yarn install`
- Run app in Tizen emulador: `$ yarn run:tizen`

## Run app in emulator:

`$ rnv run -p <platform>` \*platform: tvos, androidtv, tizen
