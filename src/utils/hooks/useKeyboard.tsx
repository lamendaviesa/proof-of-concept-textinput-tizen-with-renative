import { MutableRefObject, useEffect, useState } from 'react'

/* 
Documentation: 
  @see https://developer.samsung.com/smarttv/develop/guides/user-interaction/keyboardime.html
  @see Privileges: https://docs.tizen.org/application/web/tutorials/sec-privileges/
*/
export const useKeyBoard = <T,>(ref: MutableRefObject<T | null>): { log: string } => {
  const [log, setLog] = useState('')

  useEffect(() => {
    const onPressKey = (event: any) => {
      setLog(`keyCode: ${event.keyCode}, code: ${event.code}`)

      // Done (65376) or Cancel(65385)
      if (event.keyCode === 65376 || event.keyCode === 65385) {
        // @ts-ignore
        ref?.current?.blur()
      }
    }

    document.body.addEventListener('keydown', onPressKey)

    return () => {
      document.body.removeEventListener('keydown', onPressKey)
    }
  }, [])

  return { log }
}
