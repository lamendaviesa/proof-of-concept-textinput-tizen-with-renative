import { isEngineWeb, isFactorTv, isPlatformTizen, isPlatformAndroidtv, isPlatformTvos } from 'renative'

const hasWebFocusableUI = isEngineWeb && isFactorTv

export { hasWebFocusableUI, isPlatformTizen, isPlatformAndroidtv, isPlatformTvos }
