import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import { withFocusable } from '@noriginmedia/react-spatial-navigation'

interface PropsFocusable {
  onFocus?: () => void
  onBlur?: () => void
  onEnterPress?: () => void
  focused?: boolean
  style?: any
}

let TouchableFocusable: React.FC<PropsFocusable> = ({ style, onFocus, onBlur, children, focused }) => {
  const [focus, setFocus] = useState(false)

  useEffect(() => {
    if (focus !== focused) {
      setFocus(focused!)
      if (focused) {
        onFocus && onFocus()
      } else {
        onBlur && onBlur()
      }
    }
  }, [focused, onFocus, onBlur, setFocus])

  return <View style={style}>{children}</View>
}

TouchableFocusable = withFocusable()(TouchableFocusable)

interface Props {
  onFocus?: () => void
  onBlur?: () => void
  onPress?: () => void
  style?: any
}

export const Touchable: React.FC<Props> = ({ onPress, children, ...props }) => {
  return (
    <TouchableFocusable onEnterPress={onPress} {...props}>
      {children}
    </TouchableFocusable>
  )
}
