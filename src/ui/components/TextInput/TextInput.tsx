import React, { useRef, useState } from 'react'
import { StyleProp, Text, TextInput as TextInputBase, ViewStyle } from 'react-native'
import { Touchable } from 'ui/components/Touchable'
import { useKeyBoard } from 'utils/hooks/useKeyboard'
import { textInputStyles } from './TextInput.styles'

interface Props {
  placeholder?: string
  style?: StyleProp<ViewStyle>
  onBlur?: (value: string) => void
}

export const TextInput: React.FC<Props> = ({ style, onBlur, ...props }) => {
  const [value, setValue] = useState('')
  const [focused, setFocused] = useState(false)
  const ref = useRef<TextInputBase>(null)

  const { log } = useKeyBoard<TextInputBase>(ref)

  return (
    <>
      <Text style={textInputStyles.text}>{`Value : ${value}`}</Text>
      <Text style={textInputStyles.text}>{`Touched input : ${focused}`}</Text>
      <Text style={textInputStyles.text}>{`Log : ${log}`}</Text>
      <Touchable
        onFocus={() => {
          setFocused(true)
        }}
        onBlur={() => {
          setFocused(false)
        }}
        onPress={() => {
          ref?.current?.focus()
        }}>
        <TextInputBase
          {...props}
          ref={ref}
          placeholderTextColor="white"
          style={[textInputStyles.textInput, focused && textInputStyles.textInputFocused]}
          onChangeText={text => {
            setValue(text)
          }}
        />
      </Touchable>
    </>
  )
}
