import { StyleSheet } from 'react-native'

export const textInputStyles = StyleSheet.create({
  text: {
    color: 'white',
    fontSize: 40
  },
  textInput: {
    height: 80,
    color: 'white',
    borderColor: 'white',
    borderWidth: 1,
    textAlign: 'center',
    fontSize: 40,
    backgroundColor: 'grey'
  },
  textInputFocused: {
    borderColor: 'blue',
    borderWidth: 4
  }
})
