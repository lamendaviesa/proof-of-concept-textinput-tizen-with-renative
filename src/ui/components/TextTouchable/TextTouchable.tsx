import React, { useState } from 'react'
import { Text } from 'react-native'
import { Touchable } from 'ui/components/Touchable'

export const TextTouchable: React.FC = ({ children }) => {
  const [focus, setFocus] = useState(false)
  return (
    <Touchable
      onFocus={() => {
        setFocus(true)
      }}
      onBlur={() => {
        setFocus(false)
      }}>
      <Text
        style={{
          fontSize: 40,
          textAlign: 'center',
          borderRadius: 20,
          color: focus ? 'red' : 'white',
          borderColor: focus ? 'red' : 'white',
          borderWidth: focus ? 4 : 1
        }}>
        {children}
      </Text>
    </Touchable>
  )
}
