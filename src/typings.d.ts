declare module 'renative'

declare module '@noriginmedia/react-spatial-navigation' {
  import { Props } from 'react'

  type Direction = 'left' | 'right' | 'up' | 'down'

  export interface Layout {
    height: number
    left: number
    node: HTMLElement
    top: number
    width: number
    x: number
    y: number
  }

  export interface FocusableProps {
    focused?: boolean
    hasFocusedChild?: boolean
    navigateByDirection?: (direction: Direction) => void
    parentFocusKey?: string
    pauseSpatialNavigation?: () => void
    preferredChildFocusKey?: string
    realFocusKey?: string
    resumeSpatialNavigation?: () => void
    setFocus?: (focus?: string) => void
    stealFocus?: () => void
  }

  export interface FocusableWrapperProps {
    children?: ReactNode
    focusable?: boolean
    focusKey?: string
    forgetLastFocusedChild?: boolean
    onArrowPress?(direction: Direction, props?: Props): boolean
    onBecameFocused?(layout: Layout, props?: Props): void
    onEnterPress?(props?: Props): void
    trackChildren?: boolean
  }

  export interface WithFocusableOptions {
    forgetLastFocusedChild?: boolean
    trackChildren?: boolean
  }

  export interface InitNavigationParams {
    debug?: boolean
    nativeMode?: boolean
    throttle?: number
    visualDebug?: boolean
  }

  export function initNavigation(params?: InitNavigationParams): void

  export function setKeyMap({ [string]: number }): void

  export function withFocusable(WithFocusableOptions?: WithFocusableOptions): React.ReactComponentElement
}
