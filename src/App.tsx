import { FocusableProps, initNavigation, withFocusable } from '@noriginmedia/react-spatial-navigation'
import React, { useEffect } from 'react'
import { View } from 'react-native'
import { TextInput } from 'ui/components/TextInput'
import { TextTouchable } from 'ui/components/TextTouchable'
import { hasWebFocusableUI, isPlatformTizen } from 'utils/config/platform'

if (hasWebFocusableUI) {
  initNavigation()
}

let App = ({ setFocus }: FocusableProps) => {
  useEffect(() => {
    if (isPlatformTizen && window.focus) window.focus()
  }, [])

  useEffect(() => {
    setFocus && setFocus()
  }, [])

  return (
    <View style={{ flex: 1, backgroundColor: '#101010' }}>
      <TextTouchable>Touch me!</TextTouchable>
      <TextInput placeholder="Input, press me!" />
      <TextTouchable>Touch me!</TextTouchable>
    </View>
  )
}

App = withFocusable()(App)

export { App }
