const { withRNV } = require('@rnv/engine-rn');
const path = require('path');

const defaultConfig = {};

module.exports = withRNV(defaultConfig);
