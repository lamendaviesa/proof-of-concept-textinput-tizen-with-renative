module.exports = {
  preset: 'react-native',
   clearMocks: true,
  'transformIgnorePatterns': [
    'node_modules/(?!react-native)/'
  ],
};
